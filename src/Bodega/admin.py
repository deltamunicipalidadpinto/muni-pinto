from django.contrib import admin

# Register your models here.

from .models import *

class AdminBodega(admin.ModelAdmin):
    list_display = ["ID", "nombre_producto", "cantidad"]
    #form = RegModelForm
    list_filter = ["fecha_ingreso"]

    # class Meta:
    # model = Bodega


# Register your models here.

admin.site.register(Bodega, AdminBodega)