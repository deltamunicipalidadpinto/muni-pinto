from django.apps import AppConfig


class BodegaConfig(AppConfig):
    name = 'Bodega'
