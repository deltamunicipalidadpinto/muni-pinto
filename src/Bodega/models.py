from django.db import models

# Create your models here.
class Bodega(models.Model):
 ID = models.AutoField(primary_key=True, unique=True)
 nombre_producto = models.CharField(max_length=100, blank=True, null=True)
 cantidad = models.IntegerField()
 stock_minimo = models.IntegerField()
 ubicacion = models.CharField(max_length=100, blank=True, null=True)
 fecha_ingreso = models.DateTimeField(auto_now_add=True, auto_now=False)


 def __str__(self):
    return self.nombre

