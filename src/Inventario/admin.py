from django.contrib import admin

# Register your models here.
from .models import *

class AdminInventario(admin.ModelAdmin):
    list_display = ["nombre_producto", "cantidad", "pertenencia_primaria","pertenencia_secundaria"]
    #form = RegModelForm
    list_filter = ["ID", "nombre_producto"]

    # class Meta:
    # model = Bodega


# Register your models here.

admin.site.register(Inventario, AdminInventario)