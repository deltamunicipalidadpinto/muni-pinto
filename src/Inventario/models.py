from django.db import models

# Create your models here.
class Inventario(models.Model):
    ID = models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=100, blank=True, null=True)
    cantidad = models.IntegerField()
    pertenencia_primaria = models.CharField(max_length=100, blank=True, null=True)
    pertenencia_secundaria = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.nombre_producto