from django import forms
from .models import *

class RegModelForm(forms.ModelForm):
 class Meta:
     modelo = Notebook
     campos = ["nombre", "descripcion", "categoria", "sub_categoria"]

class RegForm(forms.Form):
 # RegForm es el nombre para el registro del formulario
 nombre = forms.CharField(max_length=100)
 descripcion = forms.CharField(max_length=150)
 categoria = forms.CharField(max_length=150)
 sub_categoria = forms.CharField(max_length=100)

 def clean_nombre(self):
     nombre = self.cleaned_data.get("nombre")
     if nombre:
         if len(nombre) <= 2 and len(nombre) <= 100:
             raise forms.ValidationError("El campo nombre debe ser mayor a dos caracteres")
         return nombre
     else:
         raise forms.ValidationError("Debe ingresar al menos un caracter")
