from django.contrib import admin

# Register your models here.
from .models import *

class AdminNotebook(admin.ModelAdmin):
    list_display = ["nombre", "descripcion", "categoria"]
    #form = RegModelForm
    list_filter = ["nombre", "descripcion", "categoria"]

    # class Meta:
    # model = Bodega


# Register your models here.

admin.site.register(Notebook, AdminNotebook)