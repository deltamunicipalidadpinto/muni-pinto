from django.shortcuts import render

# Create your views here.
from .forms import *
from .models import *
# Create your views here.

def Producto_view(request):
    form = RegForm(request.POST or None)
    contexto = {
        "el_formulario": form,
    }

    if form.is_valid():
        form_data = form.cleaned_data

        nombre = form_data.get("nombre")
        descripcion = form_data.get("descripcion")
        categoria = form_data.get("categoria")
        sub_categoria = form_data.get("sub_categoria")
        objeto = Notebook.objects.create(nombre=nombre, descripcion=descripcion, categoria=categoria, sub_categoria=sub_categoria)


    return render(request, "Notebook.html", contexto)