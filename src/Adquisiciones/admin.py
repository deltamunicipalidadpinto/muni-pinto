from django.contrib import admin

# Register your models here.
from .models import *

class AdminRegistro(admin.ModelAdmin):
    list_display = ["nombre_producto", "nombre_proveedor", "cantidad"]
    #form = RegModelForm
    list_filter = ["fecha_compra"]

    # class Meta:
    # model = Bodega


# Register your models here.

admin.site.register(Registro, AdminRegistro)