from django.shortcuts import render

# Create your views here.
from .forms import *
from .models import *
# Create your views here.

def Adquisicion_view(request):
    form = RegForm(request.POST or None)
    contexto = {
        "el_formulario": form,
    }

    if form.is_valid():
        form_data = form.cleaned_data
        nombre_producto = form_data.get("nombre_producto")
        nombre_proveedor = form_data.get("nombre_proveedor")
        cantidad = form_data.get("cantidad")
        numero_factura = form.data.get("numero_factura")
        objeto = Registro.objects.create(nombre_producto=nombre_producto, nombre_proveedor=nombre_proveedor, cantidad=cantidad, numero_factura=numero_factura)

    return render(request, "Registro.html", contexto)