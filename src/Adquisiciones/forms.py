from django import forms
from .models import *

class RegModelForm(forms.ModelForm):
 class Meta:
     modelo = Registro
     campos = ["nombre_producto", "nombre_proveedor", "cantidad", "numero_factura"]

class RegForm(forms.Form):
 # RegForm es el nombre para el registro del formulario
 nombre_producto = forms.CharField(max_length=100)
 nombre_proveedor = forms.CharField(max_length=150)
 cantidad = forms.CharField(max_length=150)
 numero_factura = forms.IntegerField()

 def clean_nombre_prod(self):
     nombre_producto = self.cleaned_data.get("nombre_producto")
     if nombre_producto:
         if len(nombre_producto) <= 2 and len(nombre_producto) <= 100:
             raise forms.ValidationError("El campo nombre debe ser mayor a dos caracteres")
         return nombre_producto
     else:
         raise forms.ValidationError("Debe ingresar al menos un caracter")

 def clean_nombre_prov(self):
     nombre_proveedor = self.cleaned_data.get("nombre_proveedor")
     if nombre_proveedor:
         if len(nombre_proveedor) <= 2 and len(nombre_proveedor) <= 100:
             raise forms.ValidationError("El campo nombre debe ser mayor a dos caracteres")
         return nombre_proveedor
     else:
         raise forms.ValidationError("Debe ingresar al menos un caracter")