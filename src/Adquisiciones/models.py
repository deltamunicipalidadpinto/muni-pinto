from django.db import models

# Create your models here.
class Registro(models.Model):
 identificador = models.AutoField(primary_key=True, unique=True)
 nombre_producto = models.CharField(max_length=100, blank=True, null=True)
 nombre_proveedor = models.CharField(max_length=100, blank=True, null=True)
 cantidad = models.IntegerField()
 fecha_compra = models.DateTimeField(auto_now_add=True, auto_now=False)
 numero_factura = models.IntegerField()

 def __int__(self):
     return self.identificador